const { getCallStepsListRequest, changeCallLogsStatusRequest, createCallChatLogsRequest, getCallLogsStatisticsRequest } = require('./services.js');
const { readSettings, saveSettings, getAccessToken, HeartBeatCheck, OnSendMessage } = require('./utils.js');
const {ipcRenderer} = require('electron');
const EventEmitter = require('events').EventEmitter;  
const {shell} = require('electron'); 
var nconf = require('nconf').file({
    file: getUserHome() + '/config.json'
});
const request = require('superagent');
const fs = require('fs');
const path = require("path");

const emitter = new EventEmitter();
var webSocket;


var debug = false;
function getUserHome() {
    return process.env[(process.platform == 'win32') ? 'USERPROFILE': 'HOME'];
}

var src_crmurl = readSettings('crmurl');
/*var src_username = readSettings('username');
var src_password = readSettings('password');*/
var src_token = readSettings('token');
var src_crmlogin = readSettings('crmlogin');
var src_autophone = readSettings('autophone');
var src_appkey = readSettings('appkey');
var src_accessKey = readSettings('accessKey');
var src_accessSecret = readSettings('accessSecret');
// var src_crmaudio = readSettings('crmaudio');
// var src_notifylogin = readSettings('notifylogin')
// var src_rate = readSettings('rate');
//var appKey = 'XscjJXsviPbtQssk';
//var appToken = '3483226461c2472187d61ee243314e3e';
var userid = 0;
var allPages = ['loginpage', 'phone', 'debugphone', 'updatephone', 'displaylog','tts'];
//recgresult.indexOf('是')>-1 || recgresult.indexOf('好')>-1 || recgresult.indexOf('可以')>-1 || recgresult.indexOf('要')>-1 || recgresult.indexOf('有')>-1 || recgresult.indexOf('钱')>-1 || recgresult.indexOf('多少')>-1){
var yeswords = [];//'钱', '多少', '小区', '物业','房子'
var nowords = [];
var questions = new Array();

var callsteps = new Array();


var callresult = new Array();
var callrecords = new Array();
var curstep = 0; //当前打电话的步骤
var calltimes = 0;
let firstCallSteps = [];
let chatLog = {
    content: ``,
    iscustomer: 0,
    type: '',
    callee: 0,
    calllogsid: 0
};
let serialNumber = 0;
let playEndTime = 0;
let allowBreakTime = 0;
let isbreak = false;//是否允许打断
let huashuname = '';//话术名称
let isconnected = false;
let logstatus = 0;
let notconnected = {
    'poweroff': ['关机','挂机'],
    'notexist': ['空号'],
    'notavailable': ['无法接通']
};
let notconnectedStatus = {
    'poweroff': 2,
    'notexist': 3,
    'notavailable': 5

};
//0=未开始，1=外呼中，2=呼出，对方未振铃，可能是空号，3=呼出，对方未接听，4=接通， 5=呼入，未接来电，6已关机
//0=未开始，1=外呼中，2=关机，3=空号，4=接通， 5=无法接通,6未知
window.onload = function() {
    document.getElementById("crmurl").value = src_crmurl != '' ? src_crmurl: "";
/*    document.getElementById("username").value = src_username != '' ? src_username: "";
    document.getElementById("password").value = src_password != '' ? src_password: "";*/
    document.getElementById("token").value = src_token != '' ? src_token: "";
    document.getElementById("appkey").value = src_appkey != '' ? src_appkey: "";
    document.getElementById("accessKey").value = src_accessKey != '' ? src_accessKey: "";
    document.getElementById("accessSecret").value = src_accessSecret != '' ? src_accessSecret: "";
    if (src_crmlogin != '' && src_crmlogin == true) {
        document.getElementById("crmlogin").checked = true;
    }
    if (src_autophone != '' && src_autophone == true) {
        document.getElementById("autophone").checked = true;
    }
    // if(src_crmaudio != '' && src_crmaudio == true) {
    //   document.getElementById("crmaudio").checked = true;
    // }
    // if(src_notifylogin != '' && src_notifylogin == true) {
    //   document.getElementById("notifylogin").checked = true;
    // }
    // if(src_rate != '') {
    //   document.getElementById("rate").value = src_rate;
    // }

    // document.getElementById("debug").onclick = function () {
    //   ipcRenderer.send('open-debugwindow');
    // }
    // document.getElementById("open").onclick = function () {
    //   ipcRenderer.send('open-window');
    // }
    ipcRenderer.send('submit-main-window');
    openWebSocket().then(() => {
        document.getElementById("submit").onclick = function() {
            login();
        }
        if (document.getElementById("crmlogin").checked) {
            login();
        } 
        OnOpenOnewayRecord();       
        OnOpenDevice();
    })

}

function showPage(pageName) {
    layui.use(['jquery'],
    function() {
        var $ = layui.$;
        var item;
        for (item in allPages) {
            if (allPages[item] != pageName) {
                var result = $("#" + allPages[item]).hide();
            }
        }
/*        if (pageName == 'phone' || pageName == 'debugphone' || pageName == 'updatephone') {
            $("#displaylog").show();
        }*/
        $("#" + pageName).show();
    });
}

ipcRenderer.on('showPage',
function(event, pageName) {
    console.log(pageName);
    console.log(event);
    showPage(pageName);
});

const openWebSocket = () => {
    return new Promise((resolve, reject) => {
        webSocket = new WebSocket('ws://127.0.0.1:8555/api');
        webSocket.onerror = function(event) {
            reject();
        };
        webSocket.onclose = (event) => {
            webSocket = new WebSocket('ws://127.0.0.1:8555/api');
            webSocket.onopen = function (event) {
                window.setInterval(HeartBeatCheck, 10000);
                document.getElementById('messages').innerHTML += '<br />与服务器端建立连接';
                setTimeout('OnOpenSpeechRecogn()', 600);
                //setTimeout('OnGetSpeechRecogn()', 600);

            };
        }
        //与WebSocket建立连接
        webSocket.onopen = function (event) {
            window.setInterval(HeartBeatCheck, 10000);
            document.getElementById('messages').innerHTML += '<br />与服务器端建立连接';
            setTimeout('OnOpenSpeechRecogn()', 600);
            //setTimeout('OnGetSpeechRecogn()', 600);
            resolve();
        };
        //处理服务器返回的信息
        webSocket.onmessage = function(event) {
            console.log(event.data);
            var msgJsonData;
            layui.use(['jquery'],
            function() {
                var $ = layui.$;
                msgJsonData = $.parseJSON(event.data);
            });
            var msg;
            console.log(msgJsonData);
            if(msgJsonData.message != undefined) msg = msgJsonData.message;
            if (msgJsonData.type == "InstructionTrace") {
                document.getElementById('trace').innerHTML += '<br />' + msg;
            } else if (msgJsonData.type == "RealTimeState") {
                document.getElementById('device-state').innerHTML += '<br />' + msg;
                if (msgJsonData.dynamicdata.realtimestate == 'incoming') { //设备呼入
                    //setTimeout('OnAnswer()', 600);
                    
                } else if (msgJsonData.dynamicdata.realtimestate == 'hangup') {
                    console.log('realtimestate isHangup')
                    emitter.emit('phone.websocket.hangup')
                } else if (msgJsonData.dynamicdata.realtimestate == 'ringback') {

                } else if (msgJsonData.dynamicdata.realtimestate == 'outconnected') { //外呼接通
                    emitter.emit('phone.websocket.outconnected');

                } else if (msgJsonData.dynamicdata.realtimestate == 'inconnected') {

                    if (firstCallSteps.voice != undefined) {
                        console.log('aaaaaaaaaaaaaa');
                        console.log('0000 voice file:' + firstCallSteps.voice);
                        PlayRecord(firstCallSteps.voice);
                        callresult[calltimes] = firstCallSteps.steptext;
                        chatLog.content += `#caller#${firstCallSteps.steptext}<br/>`;
                        calltimes++;
                        curstep = firstCallSteps.id;
                        callrecords.push(firstCallSteps.voice);
                        let time = firstCallSteps.voicetime;
                        playEndTime = new Date().getTime() + parseInt(time * 1000);
                        allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);

                        calcOvertime(time);
                    }
          
                } else if (msgJsonData.dynamicdata.realtimestate == 'outgoing') { //设备呼出
 
                }
            } else if (msgJsonData.type == 'CallRecord') {
                
                let dynamicdata = msgJsonData.dynamicdata;
                let duringtime = 0;
                if (isconnected == false) {
                    let timeout = 10;
                    let time = 0;
                    (function poll() {
                      if(++time > timeout * 1000 / 4){
                        emitter.emit('phone.websocket.CallRecord',duringtime, 6);
                      };
                      logstatus > 0 ? emitter.emit('phone.websocket.CallRecord',duringtime, logstatus) : setTimeout(poll, 4);
                    }());
                        
                    
                } else {
                    logstatus = 4;
                    duringtime = (new Date(dynamicdata.endtime).getTime() -new Date(dynamicdata.answertime).getTime()) / 1000;
                    emitter.emit('phone.websocket.CallRecord',duringtime, logstatus);
                }
                
            } else if (msgJsonData.type == "SpeechRecogn") {
                console.log(msgJsonData.dynamicdata);
                console.log('dynamicdata.result length:'+ msgJsonData.dynamicdata.result.length)
                let dynamicdata = msgJsonData.dynamicdata;
                let recgresult = dynamicdata.result;
                if (dynamicdata != null && dynamicdata.state && recgresult.length >= 2) {
                    chatLog.content += `#callee#${msgJsonData.dynamicdata.result}<br/>`;
                    if (isconnected == true && (isbreak == true && dynamicdata.timespace > allowBreakTime || isbreak == false && dynamicdata.timespace > playEndTime) && curstep > 0) {

                        clearTimeout(overtimeTimer);//清除超时定时器
                        document.getElementById('messages').innerHTML += '<br />' + msgJsonData.dynamicdata.result;
                        
                        callresult[calltimes] = recgresult;
                        calltimes++;
                        //否定回答后挂机
                        if (isExists(nowords, recgresult)) {
                            emitter.emit('phone.websocket.SpeechRecogn.no');
                            //收到肯定回答后播放下一条肯定的录音
                        } else if (isExists(yeswords, recgresult)) { //recgresult.indexOf('是')>-1 || recgresult.indexOf('好')>-1 || recgresult.indexOf('可以')>-1 || recgresult.indexOf('要')>-1 || recgresult.indexOf('有')>-1 || recgresult.indexOf('钱')>-1 || recgresult.indexOf('多少')>-1){
                            emitter.emit('phone.websocket.SpeechRecogn.yes')

                        } else {
                            //根据问题的关键字播放录音
                            var isAnswer = false;
                            for (var i = 0; i < questions.length; i++) {
                                question = questions[i];
                                var keywords = question.keywords;
                                console.log('keywords:' + keywords);
                                var wordArr = keywords.split(',');
                                console.log(wordArr);
                                if (isExists(wordArr,recgresult)) {
                                    emitter.emit('phone.websocket.SpeechRecogn.isAnswer');
                                    break;
                                }
                            }
                            //没有合适的问题回复，表示没有识别出关键问题来
                            if (!isAnswer) {
                            
                                emitter.emit('phone.websocket.SpeechRecogn.noAnswer');
                            }

                        }
                        
                    } else if (isconnected == false){
                        emitter.emit('phone.websocket.SpeechRecogn.notconnected', recgresult);
                    }
                }
                
            } else if (msgJsonData.type == "CommandResponse") {
                var invokeCommand = msgJsonData.data.invoke_command.toLowerCase();
                let cid = msgJsonData.data.cid;
                if (cid) {
                    let req = promisePool[cid];
                    req.resolve(msgJsonData);
                    delete promisePool[cid];
                } else {
                    if (invokeCommand == "OpenSpeechRecogn") {
                        setTokenDial(src_accessKey, src_accessSecret,true);
                    }
                    // if (invokeCommand == "getspeechrecognenbale") {
                    //     if (msgJsonData.data.state) {
                    //         document.getElementById('speechrecognenable').innerHTML = "打开";
                    //         getAccessToken(src_accessKey, src_accessSecret).then(apptoken => {
                    //             OnSetSpeechRecogn(apptoken) ;
                    //         })
                    //     } else {
                    //         document.getElementById('speechrecognenable').innerHTML = "关闭";
                    //         webSocket.send('{"command":"OpenSpeechRecogn"}');
                    //     }
                    // } else if (invokeCommand == "OpenSpeechRecogn") {
                    //     getAccessToken(src_accessKey, src_accessSecret).then(apptoken => {
                    //         OnSetSpeechRecogn(apptoken);
                    //     })
                    // } else if (invokeCommand == 'SendMessage') {

                    // }                            
                }


            } else {
                document.getElementById('messages').innerHTML += '<br />' + msg;
            }
        };                        
    })
}

var src_apptoken = '';
function login() {
    var crmurl = document.getElementById("crmurl").value;
/*    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;*/
    var token = document.getElementById("token").value;
    var crmlogin = document.getElementById("crmlogin").checked;
    var autophone = document.getElementById("autophone").checked;
    var appkey = document.getElementById("appkey").value;
    var accessKey = document.getElementById("accessKey").value;
    var accessSecret = document.getElementById("accessSecret").value;

    console.log('autophone:' + autophone);
    // var crmaudio = document.getElementById("crmaudio").checked;
    // var notifylogin = document.getElementById("notifylogin").checked;
    // var rate = document.getElementById("rate").value;
    if (crmurl == '') {
        var options = [{
            title: "温馨提醒",
            body: "网址不能为空!"
        }];
        var nativeNotification = new Notification(options[0].title, options[0]);
        document.getElementById("crmurl").focus();
        return;
    } else {
        var tomatch = /http:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/
        if (!tomatch.test(crmurl)) {
            var options = [{
                title: "温馨提醒",
                body: "网址格式不对!"
            }];
            var nativeNotification = new Notification(options[0].title, options[0]);
            document.getElementById("crmurl").focus();
            return;
        }
    }
    if (token == '') {
        var options = [{
            title: "温馨提醒",
            body: "token不能为空!"
        }];
        var nativeNotification = new Notification(options[0].title, options[0]);
        document.getElementById("token").focus();
        return;
    }

    src_crmurl = crmurl;
/*    src_username = username;
    src_password = password;*/
    src_token = token;
    src_crmlogin = crmlogin;
    src_autophone = autophone;
    src_appkey = appkey;
    src_accessKey = accessKey;
    src_accessSecret = accessSecret;
    // src_crmaudio = crmaudio;
    // src_notifylogin = notifylogin;
    // src_rate = rate;
    saveSettings('crmurl', crmurl);
/*    saveSettings('username', username);
    saveSettings('password', password);*/
    saveSettings('token', token);
    saveSettings('crmlogin', crmlogin);
    saveSettings('autophone', autophone);
    saveSettings('appkey', appkey);
    saveSettings('accessKey', accessKey);
    saveSettings('accessSecret', accessSecret);
    // saveSettings('crmaudio', crmaudio);
    // saveSettings('notifylogin', notifylogin);
    // saveSettings('rate', rate);

    GetSerialNumber().then((res) => {
        if (res.data.state == false) {
            throw new Error(res.message);
        }
        serialNumber = res.message;
        saveSettings('serialnumber', serialNumber);
        //var p1 = getAccessToken(accessKey, accessSecret);
        var p1 = setTokenDial(accessKey, accessSecret, true);        
        var url = src_crmurl + '/rest.php?method=checkToken&rest_data=[{"token":"' + src_token + '","serialnumber":"' + serialNumber + '"}]';
        //console.log(url);
        var p2 = request.post(url).set('Accept', 'application/json');
        Promise.all([p1, p2]).then(function(result){
            src_apptoken = result[0];
            let res = result[1];
            if (res.ok) {
                console.log(res.text);
                var str = res.text.trim().substring(res.text.indexOf('(') + 1, res.text.lastIndexOf(')'));
                var result = JSON.parse(str);
                console.log(result);
                if (result != 'Login failed' && result.id > 0) {
                    userid = result.id;
                    saveSettings('userid', userid);
                    console.log('Login successfully ! the userid is ' + result.id);
                    showPage('phone');
                    //var autophone = true;             
                }
            }

        });
    })
    .catch((e)=> {
        layer.msg(e.message);
    })

}


function Record() {
    OnStartRecord();
    document.getElementById('trace').innerHTML += '<br />开始录音';
    setTimeout(function() {
        OnStopRecord();
    },
    10000)
}
function ClearMsg() {
    document.getElementById('messages').innerHTML = '';
    document.getElementById('device-state').innerHTML = '';
    document.getElementById('trace').innerHTML = '';
}
function Play() {
    let audio = new Audio();
    audio.src = "./syAudio.wav";
    audio.play();
}

function PlayRecord(filename) {
    //var filename = document.getElementById("recordFile").value;
    var filePath = '';
    console.log('record file:'+filename);
    if(filename == '' || filename == 'null'){
        return ;
    }
    if (debug == true) {
        filePath = path.resolve(__dirname,'../voice', filename + '.wav');
    } else {
        filePath = path.resolve(__dirname,'../../app.asar.unpacked/voice', filename + '.wav');
    }
    if (fs.existsSync(filePath)) {
        webSocket.send('{"command":"PlayStartRecord","arguments":{"content":"' + filePath + '"}}');
    } else {
        let url = src_crmurl + '/download.php?attachmentsid=' + Buffer.from(filename).toString('base64');
        request.get(url).then(response => {
            if (response.ok) {
                let body = response.body;
                fs.writeFileSync(filePath, body);
                console.log('The GET request is succeed!');
                webSocket.send('{"command":"PlayStartRecord","arguments":{"content":"' + filePath + '"}}');
            }
        })
    }
  
    //console.log('PlayStartRecord start');
    //webSocket.send('{"command":"PlayStartRecord","arguments":{"content":"' + filePath + '"}}');
    //console.log('PlayStartRecord end');
}

function Debug() {
    ipcRenderer.send('open-debugwindow');
}

function OnGetConnectedState() {
    webSocket.send('{"command":"GetConnectedState"}');
}
function OnResetToFactoryConfig() {
    layer.confirm('确认恢复吗？',
    function(index) {
        webSocket.send('{"command":"ResetToFactoryConfig"}');
        layer.close(index);
        //向服务端发送删除指令
    });

}
function OnGetDeviceState() {
    webSocket.send('{"command":"GetDeviceState"}');
}
function OnSendDTMF() {
    layer.prompt(function(value, index, elem) {
        webSocket.send('{"command":"SendDTMF","arguments":{"content":"' + value + '"}}');
        layer.close(index);
    });
    //var dtmf = document.getElementById('dtmf').value;      
}
/*function OnSendMessage() {
    var number = document.getElementById('smsnumber').value;
    var content = document.getElementById('messageContent').value;
    if (number == '') {
        layer.msg('电话不能为空');
        return;
    }
    if (content == '') {
        layer.msg('短信内容不能为空');
        return;
    }
    webSocket.send('{"command":"SendMessage","arguments":{"phone":"' + number + '","content":"' + content + '"}}');
    //document.getElementById('number').value = '';
    document.getElementById('messageContent').value = '';

}*/
function OnUpdateDevice() {
    var filename = document.getElementById("file").value;
    if (filename == '') {
        layer.msg('请选择新版本的固件文件');
        return;
    }
    if (filename.indexOf('.bin') < 0) {
        layer.msg('请选择正确的固件文件');
        return;
    }
    layer.confirm('确认升级设备固件吗？',
    function(index) {
        var resultFile = document.getElementById("file").files[0];
        if (resultFile.path != undefined) {
            document.getElementById("file").value = '';
            webSocket.send('{"command":"UpdateDevice","arguments":{"content":"' + resultFile.path + '"}}');
            layer.close(index);
        }
    });

}
function OnGetVersion() {
    webSocket.send('{"command":"GetVersion"}');
}
async function changeCallLogsStatus(callLogsid, status) {
    let response = await changeCallLogsStatusRequest(callLogsid, status).catch(e => console.log(e));
    console.log(response);
}


let callStepsList = [];
async function getCallStepsList() {
    console.log('start getCallStepsListRequest');
    let list = await getCallStepsListRequest().catch(e => console.log(e));
    console.log(list);
    console.log('end getCallStepsListRequest');
    if (list.ok) {
        console.log(list.body);
        callStepsList = callStepsList.concat(list.body);
/*        for (item of data) {
            var number = item.phone;
            if (number == '') {
                layer.msg('电话不能为空');
                return;
            }
            webSocket.send('{"command":"DailOut","arguments":{"phone":"' + number + '"}}');
        }*/


    }
    
}

async function createCallChatLogs(chatLog) {
    await createCallChatLogsRequest(chatLog).catch(e=>console.log(e));
}
let queueTimer = null;
let queueTime = 30 * 1000;
let requestTime = 3*60*1000;
let requestTimer = null;
let isStop = false;
function OnWaiHu() {
    clearInterval(queueTimer);
    queueTimer = setInterval(function () {
        emitter.emit('phone.queue.start');
    }, queueTime);
    getCallStepsList();
    requestTimer = setInterval(getCallStepsList, requestTime);
    layui.$('.callstatus').text('正在外呼中...');
    layui.$('#startWaihu').addClass('layui-btn-disabled').prop('disabled', true);
    layui.$('#stopWaihu').removeClass('layui-btn-disabled').prop('disabled', false);
}

function StopWaiHu(elem) {

    clearInterval(queueTimer);
    clearInterval(requestTimer);
    GetCallState().then(function(res){
        let msg = res.message;
        layui.$('#stopWaihu').addClass('layui-btn-disabled').prop('disabled', true);
        if (msg == 'CALL_STATE_IDLE') {
            layui.$('.callstatus').text('暂停中...');
            layui.$('#startWaihu').removeClass('layui-btn-disabled').prop('disabled', false);
        } else {
            layer.msg('当前外呼电话挂断后，将会停止，请稍后...');
            isStop = true;
            layui.$('.callstatus').text('正在暂停中，请稍后...');            
        }
    })
}

function OnDailout(item) {
    var number = item.phone;
    if (number == '') {
        layer.msg('电话不能为空');
        return;
    }
/*    webSocket.send({
        "command":"DailOut",
        "cid": getCid(),
        "arguments":{
            "phone": number 
        }
    });*/
    //document.getElementById('number').value = '';

    webSocket.send(JSON.stringify({
        "command":"Dial",
        "arguments":{
            "phone": number 
        }
    }));
}

let specialSteps = {
    first: 0,
    overtime: 0,
    norecstep: 0,
    invite: 0,
};
let config = {
    issms: 0,
    smscontent: '',
    phone: ''
};
let callNum = 0;
async function getCallLogsStatistics () {
    let list = await getCallLogsStatisticsRequest();
    
    let $ = layui.$;
    if (list.ok) {
        let callRecordNum = list.body;
        console.log(callRecordNum);
        $('.task').text(callRecordNum.task);   
        $('.called').text(callRecordNum.called);
        $('.notconnected').text(callRecordNum.notconnected);
        $('.connected').text(callRecordNum.connected);
        $('.rating').text(callRecordNum.rating);
        $('.notrating').text(callRecordNum.notrating);
        $('.callnum').text(callNum);
    }
   
}

function startDailout () {
    console.log('startDailout start');
    console.log(callStepsList);
    logstatus = 0;
    isconnected = false;
    let item = callStepsList.shift();
    config.issms = item.issms;
    config.smscontent = item.smscontent;
    config.phone = item.phone;
    huashuname = item.huashuname;
    chatLog.type = item.type;
    chatLog.callee = item.callee;
    chatLog.calllogsid = item.calllogsid;
    callsteps = item['callstep']['list'];
    let firstId = item['callstep']['first'];
    specialSteps.first = firstId;
    specialSteps.overtime = item['callstep']['overtime'];
    specialSteps.invite = item['callstep']['invite'];
    firstCallSteps = item['callstep']['list'][firstId];
    questions = item['question'];
    let customParams = item['customParams'];
    yeswords = customParams['yes'];
    nowords = customParams['no'];
    isbreak = item.isbreak == 0 ? false : true;
    if (item.type == 'cluereg') {
        layui.$('.callingnow').text(`线索：${item.name}(${item.phone})`);
    } else {
        layui.$('.callingnow').text(`学员：${item.name}(${item.phone})`);
    }

    if (Object.keys(callsteps).length > 0) {
        callNum++;
        getCallLogsStatistics();
        OnDailout(item);   
    } else {
        console.log('话术流程不存在');
        emitter.emit('phone.queue.end');
    }
    console.log('startDailout end');
           
}
let overtimeTimer = null;
const calcOvertime = (voicetime) => {
    console.log((Math.ceil(voicetime) +  15 )* 1000);
    overtimeTimer = setTimeout(function () {
        PlayRecord(callsteps[specialSteps.overtime].voice);
        chatLog.content += `#caller#${callsteps[specialSteps.overtime].steptext}<br/>`;
        var time = callsteps[specialSteps.overtime].voicetime * 1000;
        setTimeout(function() {
            OnHungUp();
        },time);            
        

    }, (Math.ceil(voicetime) +  15 )* 1000);

}
//开始循环呼叫
emitter.on('phone.queue.start', function() {
    setTokenDial(src_accessKey, src_accessSecret);
    callrecords = [];
    clearInterval(queueTimer);    
    if (callStepsList.length > 0) {
       startDailout();        
    } else {
        getCallLogsStatistics();
        emitter.emit('phone.queue.end');
    }
    
});
emitter.on('phone.queue.end', function() {
    queueTimer = setInterval(function () {
        emitter.emit('phone.queue.start');
    }, queueTime);

});
emitter.on('phone.websocket.hangup', function() {
    // getTokenDial(src_accessKey, src_accessSecret).then(apptoken => {
    //     SetAppKeyToken(apptoken);            
    // });
});

emitter.on('phone.websocket.SpeechRecogn.yes', () => {
    if(callsteps[curstep] != undefined && callsteps[curstep].yesstep != undefined) {
        var nextstep = callsteps[curstep].yesstep;
        if (callsteps[nextstep] != undefined && callsteps[nextstep].voice != undefined && callsteps[nextstep].voice != '') {
            if(!isInArray(callrecords,callsteps[nextstep].voice)) {
                if (curstep == specialSteps['invite']) {
                    document.getElementById('messages').innerHTML += '<br />' + '客户状态：有意愿';
                    chatLog.iscustomer = 1;
                }
                console.log('yes voice file:' + callsteps[nextstep].voice);
                PlayRecord(callsteps[nextstep].voice);
                callrecords.push(callsteps[nextstep].voice);
                console.log('ccccccccccccc');
                callresult[calltimes] = callsteps[nextstep].steptext;
                chatLog.content += `#caller#${callsteps[nextstep].steptext}<br/>`;
                calltimes++;
                curstep = nextstep;
                var time = callsteps[nextstep].voicetime;
                if (callsteps[nextstep].yesstep=='0' && callsteps[nextstep].nostep=='0') {
                    
                    setTimeout(function() {
                        OnHungUp();
                    }, time * 1000);
                } else {
                    calcOvertime(time);
                }
                playEndTime = new Date().getTime() + parseInt(time * 1000);
                allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
            } else {
                console.log('repeatstep cccccc2222 voice file:' + callsteps[repeatstep].voice);
                /*PlayRecord(callsteps[repeatstep].voice);
                callrecords.push(callsteps[repeatstep].voice);
                console.log('ccccccccccccc22222');
                callresult[calltimes] = callsteps[repeatstep].steptext;
                calltimes++;*/
                let norecstep = callsteps[curstep].norecstep;
                PlayRecord(callsteps[norecstep].voice);
                chatLog.content += `#caller#${callsteps[norecstep].steptext}<br/>`;
                var time = callsteps[norecstep].voicetime;
                setTimeout(function() {
                    OnHungUp();
                }, time * 1000);
                playEndTime = new Date().getTime() + parseInt(time * 1000);
                allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
            }
            
        }
    }
})
emitter.on('phone.websocket.SpeechRecogn.no', () => {
    var nextstep = callsteps[curstep].nostep;
    if (callsteps[nextstep] != undefined && callsteps[nextstep].voice != undefined && callsteps[nextstep].voice != '') {
        if(!isInArray(callrecords,callsteps[nextstep].voice)) {
            if (curstep == specialSteps['invite']) {
                document.getElementById('messages').innerHTML += '<br />' + '客户状态：没有意愿';
                chatLog.iscustomer = 0;
            }
            console.log('no voice file:' + callsteps[nextstep].voice);
            PlayRecord(callsteps[nextstep].voice);
            callrecords.push(callsteps[nextstep].voice);
            console.log('bbbbbbbbbbbbb');
            callresult[calltimes] = callsteps[nextstep].steptext;
            chatLog.content += `#caller#${callsteps[nextstep].steptext}<br/>`;
            calltimes++;
            curstep = nextstep;
            var time = callsteps[nextstep].voicetime;
            if (callsteps[nextstep].yesstep=='0' && callsteps[nextstep].nostep=='0') { 
                setTimeout(function() {
                    OnHungUp();
                }, time * 1000);
            } else {
                calcOvertime(time);
            }
            playEndTime = new Date().getTime() + parseInt(time * 1000);
            allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);

        } else {       
            console.log('repeatstep bbbbbbbbbb2222 voice file:' + callsteps[repeatstep].voice);                                 
            /*PlayRecord(callsteps[repeatstep].voice);
            callrecords.push(callsteps[repeatstep].voice);
            console.log('bbbbbbbbbbbbb22222');
            callresult[calltimes] = callsteps[repeatstep].steptext;
            calltimes++;*/
            let norecstep = callsteps[curstep].norecstep;
            PlayRecord(callsteps[norecstep].voice);
            chatLog.content += `#caller#${callsteps[norecstep].steptext}<br/>`;
            var time = callsteps[norecstep].voicetime;
            setTimeout(function() {
                OnHungUp();
            }, time * 1000);
            playEndTime = new Date().getTime() + parseInt(time * 1000);
            allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
            //curstep = callsteps[nextstep].sequnce;如果这个是重复播放的录音，步骤不动
        }
    }
})
emitter.on('phone.websocket.SpeechRecogn.isAnswer', () => {
    var voicefile = question.voice;
    console.log('question voice file:' + voicefile);

    PlayRecord(question.voice);
    callrecords.push(question.voice);
    curstep = question.yesstep;
    console.log('dddddddddddddddd');
    callresult[calltimes] = question.steptext;
    chatLog.content += `#caller#${question.steptext}<br/>`;
    calltimes++;
    isAnswer = true;
    calcOvertime(question.voicetime);
    playEndTime = new Date().getTime() + parseInt(question.voicetime * 1000);
    allowBreakTime = new Date().getTime() + parseInt(question.voicetime / 3 * 1000);
})

emitter.on('phone.websocket.SpeechRecogn.noAnswer', () => {
    console.log('没有合适的问题回复，表示没有识别出关键问题来');
    console.log('curstep:'+curstep);
    console.log(callsteps[curstep]);
    var nextstep = callsteps[curstep].norecstep;
    console.log('nextstep:'+nextstep);
    if (callsteps[nextstep] != undefined && callsteps[nextstep].voice != undefined && callsteps[nextstep].voice != '') {                                        
        console.log('last voice file:' + callsteps[nextstep].voice);
        if(!isInArray(callrecords,callsteps[nextstep].voice)) {
            PlayRecord(callsteps[nextstep].voice);
            var time = callsteps[nextstep].voicetime;
            callrecords.push(callsteps[nextstep].voice);
            console.log('eeeeeeeeeeeeeeeee');
            callresult[calltimes] = callsteps[nextstep].steptext;
            chatLog.content += `#caller#${callsteps[nextstep].steptext}<br/>`;
            calltimes++;
            calcOvertime(time);
            playEndTime = new Date().getTime() + parseInt(time * 1000);
            allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
            //curstep = callsteps[nextstep].sequnce;
        } else{
            /*console.log('repeatstep eeeeeeee 222 voice file:' + callsteps[repeatstep].voice);
            PlayRecord(callsteps[repeatstep].voice);
            callrecords.push(callsteps[repeatstep].voice);
            console.log('eeeeeeeeeeee22222');
            callresult[calltimes] = callsteps[repeatstep].steptext;
            calltimes++;*/
            let norecstep = callsteps[curstep].norecstep;
            PlayRecord(callsteps[norecstep].voice);
            chatLog.content += `#caller#${callsteps[norecstep].steptext}<br/>`;
            var time = callsteps[norecstep].voicetime;
            setTimeout(function() {
                OnHungUp();
            }, time * 1000);
            playEndTime = new Date().getTime() + parseInt(time * 1000);
            allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
        }
    }
    // setTimeout(function() {
    //     OnHungUp();
    // },
    // 4000);
})
emitter.on('phone.websocket.CallRecord', function(duringtime, state) {
    let chatData = chatLog;
    chatData.duringtime = duringtime;
    chatData.state = state;
    createCallChatLogs(chatData);
    if (config.issms == 1) {
        OnSendMessage(config.phone, config.smscontent);
    }
    chatLog = {
        content: ``,
        iscustomer: 0,
        type: '',
        callee: 0,
        calllogsid: 0
    };//清空通话内容

    if (isStop) {
        layui.$('.callstatus').text('暂停中...');
        layui.$('#startWaihu').removeClass('layui-btn-disabled').prop('disabled', false);
    } else {
        if (callStepsList.length > 0) {
            startDailout();
        } else {
            emitter.emit('phone.queue.end');
        }        
    }

});
emitter.on('phone.websocket.outconnected', () => {
    isconnected = true;

    if (firstCallSteps.voice != undefined) {
        console.log('aaaaaaaaaaaaaa');
        console.log('0000 voice file:' + firstCallSteps.voice);
        PlayRecord(firstCallSteps.voice);
        callresult[calltimes] = firstCallSteps.steptext;
        chatLog.content += `#caller#${firstCallSteps.steptext}<br/>`;
        calltimes++;
        curstep = firstCallSteps.id;
        callrecords.push(firstCallSteps.voice);
        let time = firstCallSteps.voicetime;
        playEndTime = new Date().getTime() + parseInt(time * 1000);
        allowBreakTime = new Date().getTime() + parseInt(time / 3 * 1000);
        if (firstCallSteps.yesstep=='0' && firstCallSteps.nostep=='0') { 
            setTimeout(function() {
                OnHungUp();
            }, time * 1000);
        } else {
           calcOvertime(time);
        }
        
    }
})
emitter.on('phone.websocket.SpeechRecogn.notconnected', (recgresult) => {
    for (let key in notconnected) {
        let keywords = notconnected[key];
        //console.log(wordArr);
        if (isExists(keywords,recgresult)) {
            logstatus = notconnectedStatus[key];
            return;
        }
    }
    logstatus = 6;
})
/*const sendSocket = (params) => {
        let cid = params.cid;
        let str = JSON.stringify(params);
        pools[cid] = {
            resolve,
            reject
        }
        webSocket.send(str);

    
}*/

function OnAnswer() {
    webSocket.send('{"command":"Answer"}');
}

function OnHungUp() {
    webSocket.send('{"command":"HungUp"}');
    console.log(callresult);
    callresult = [];
    callrecords = [];
}

function OnGetCallState() {
    webSocket.send('{"command":"GetCallState"}');
}

let promisePool = {};
function GetCallState() {
    return new Promise(function(resolve, reject){
        let cid = new Date().getTime();
        promisePool[cid] = {
            resolve,
            reject
        }
        let msg = {
            "cid": cid,
            "command":"GetCallState"
        };
        webSocket.send(JSON.stringify(msg));
    })
    
}

function GetSerialNumber() {
    return  new Promise((resolve, reject) => {
        let cid = new Date().getTime();
        promisePool[cid] = {
            resolve,
            reject
        }        
        let msg = {
            "cid": cid,            
            "command": "GetSerialNumber"
        };
        webSocket.send(JSON.stringify(msg));
    })
}


function OnOpenDevice() {
    webSocket.send('{"command":"OpenDevice"}');
}
function OnCloseDevice() {
    layer.confirm('确认禁用设备吗？',
    function(index) {
        webSocket.send('{"command":"CloseDevice"}');
        layer.close(index);
        //向服务端发送删除指令
    });
}
function sleep(time) {
   // return new Promise((resolve) = >setTimeout(resolve, time));
}

function OnReadSms() {
    var smsindex = document.getElementById('smsindex').value;
    webSocket.send('{"command":"ReadSms","arguments":{"content":"' + smsindex + '"}}');
}
function OnPlayStartRecord() {
    webSocket.send('{"command":"PlayStartRecord"}');
}
function OnPlayStopRecord() {
    console.log('PlayStopRecord start');
    webSocket.send('{"command":"PlayStopRecord"}');
    console.log('PlayStopRecord end');
}
function OnStartRecord() {
    console.log('OnStartRecord start');
    webSocket.send('{"command":"StartRecord"}');
    console.log('OnStartRecord end');
}
function OnStopRecord() {
    console.log('StopRecord start');
    webSocket.send('{"command":"StopRecord"}');
    console.log('StopRecord end');
}

function OnGetSpeechRecogn() {
    webSocket.send('{"command":"GetSpeechRecognEnbale"}');
}
function OnSetSpeechRecogn(apptoken) {
    if (src_appkey != '' && apptoken != '') {
        webSocket.send('{"command":"SetAppKeyToken","arguments":{"appkey":"' + src_appkey + '","token":"' + apptoken + '","silence":"800"}}');
    }
}
const SetAppKeyToken = (apptoken) => {
    return new Promise((resolve, reject) => {
        if (src_appkey != '' && apptoken != '') {
            webSocket.send('{"command":"SetAppKeyToken","arguments":{"appkey":"' + src_appkey + '","token":"' + apptoken + '","silence":"800"}}');
        } else {
            throw new Error('appkey或者appToken为空');
        }       
    })
}


function OnOpenSpeechRecogn() {
    webSocket.send('{"command":"OpenSpeechRecogn"}');
    webSocket.send('{"command":"OpenOnewayRecord"}');
}
function OnCloseSpeechRecogn() {
    webSocket.send('{"command":"CloseSpeechRecogn"}');
    //webSocket.send('{"command":"CloseOnewayRecord"}');
}

function OnOpenOnewayRecord() {
    webSocket.send('{"command":"OpenOnewayRecord"}');
}
function OnCloseOnewayRecord() {
    webSocket.send('{"command":"CloseOnewayRecord"}');
}
function OnSetDeviceWindowsAudio() {
    var audio = 5;
    webSocket.send('{"command":"SetDeviceWindowsAudio","arguments":{"content":"'+ audio +'"}}');
}

const getVoiceFile = () => {

}
function startTTS() {
    var text = document.getElementById('ttsContent').value;
    var filename = new Date().getTime();
    var textUrlEncode = encodeURIComponent(text).replace(/[!'()*]/g,
    function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
    var filePath ;
    console.log(textUrlEncode);
    if (debug == true) {
        filePath = path.resolve(__dirname,'../tts');
    } else {
        filePath = path.resolve(__dirname,'../../app.asar.unpacked/tts');
    }
    fs.access(filePath, fs.constants.F_OK, function(err){
        if(err){
            fs.mkdir(filePath,function(err){
              if(err){
                console.log(err);
                return false;
              }
            })
        }

    })
    var audioSaveFile = path.resolve(filePath, filename + '.wav');
    //var audioSaveFile = 'D:/svn/electron/phonerobot/'+filename+'.wav';
    var format = 'wav';
    var sampleRate = 8000;
    getAccessToken(src_accessKey, src_accessSecret).then(apptoken => {
        processGETRequest(src_appkey, apptoken, textUrlEncode, audioSaveFile, format, sampleRate);
    })
    
    layui.$('.ttspath').text(audioSaveFile);
}

function isExists(wordArr, recgresult) {
    var isExist = false;
    for (var j = 0; j < wordArr.length; j++) {
        var word = wordArr[j];
        if (recgresult.indexOf(word) > -1) {
            isExist = true;
            break;
        }
    }
    return isExist;
}

function isInArray(arr,value){
    for(var i = 0; i < arr.length; i++){
        if(value === arr[i]){
            return true;
        }
    }
    return false;
}

let setTokenDial = async (accessKeyId, accessKeySecret,isForce=false) => {
  let timestamp = Math.round(new Date().getTime()/1000).toString();
  let accessToken = readSettings('accessToken');
  if (isForce || !accessToken || timestamp > (accessToken.ExpireTime + 5 * 60)) {
    var ROAClient = require('@alicloud/pop-core').ROAClient;
    var client = new ROAClient({
      accessKeyId: accessKeyId,
      accessKeySecret: accessKeySecret,
      endpoint: 'http://nls-meta.cn-shanghai.aliyuncs.com',
      apiVersion: '2018-05-18'
    });
    // => returns Promise
    // request(HTTPMethod, uriPath, queries, body, headers, options);
    // options => {timeout}
/*    client.request('POST', '/pop/2018-05-18/tokens').then((result) => {
      saveSettings('accessToken', result.Token);
      saveSettings('apptoken', result.Token.Id);
      console.log(result.Token);
    });   */ 
    let result = await client.request('POST', '/pop/2018-05-18/tokens');
    accessToken = result.Token;
    console.log(accessToken.Token);
    saveSettings('accessToken', accessToken);
    saveSettings('apptoken', accessToken.Id);
    SetAppKeyToken(accessToken.Id);
    OnSetDeviceWindowsAudio();//设置声音的音量为5
    return accessToken.Id;
  }

  return accessToken.Id;

}

