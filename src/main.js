const {app} = require('electron');  // Module to control application life.
const {BrowserWindow} = require('electron');  // Module to create native browser window.
const {shell} = require('electron');
const {Menu} = require('electron');
const path = require('path');
const {Tray} = require('electron');
const {ipcMain} = require('electron');
// Report crashes to our server.
// const {crashReporter} = require('electron');
// crashReporter.start();
//var remote = require('remote');
var appTray;
var trayIcon;
var timer = null;

//grunt 生成快捷方式
var handleStartupEvent = function () {
  if (process.platform !== 'win32') {
    return false;
  }
 
  var squirrelCommand = process.argv[1];
 
  switch (squirrelCommand) {
    case '--squirrel-install':
      install();
      return true;    
    case '--squirrel-updated':
      install();
      return true;
    case '--squirrel-uninstall':
      uninstall();
      app.quit();
      return true;
    case '--squirrel-obsolete':
      app.quit();
      return true;
  }
    // 安装
  function install() {
    var cp = require('child_process');    
    var updateDotExe = path.resolve(path.dirname(process.execPath), '..', 'update.exe');
    var target = path.basename(process.execPath);
    var child = cp.spawn(updateDotExe, ["--createShortcut", target], { detached: true });
    child.on('close', function(code) {
        app.quit();
    });
  }
   // 卸载
   function uninstall() {
    var cp = require('child_process');    
    var updateDotExe = path.resolve(path.dirname(process.execPath), '..', 'update.exe');
    var target = path.basename(process.execPath);
    var child = cp.spawn(updateDotExe, ["--removeShortcut", target], { detached: true });
    child.on('close', function(code) {
        app.quit();
    });
  }
 
};
 
if (handleStartupEvent()) {
  return;
}



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow ;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }
});

ipcMain.on('open-debugwindow', function (event) {
  mainWindow.webContents.openDevTools();
});
ipcMain.on('incomingReminder', function (event) {
  mainWindow.once('focus', () => mainWindow.flashFrame(false));
  mainWindow.flashFrame(true);
  var count = 0;
  timer=setInterval(function() {
      count++;
      if (count%2 == 0) {
        appTray.setImage(path.join(trayIcon, 'empty.png'));
      } else {
        appTray.setImage(path.join(trayIcon, 'crm.png'));
      }
  }, 500);//
});
ipcMain.on('submit-main-window', function (event) {
    //图标的上下文菜单
    var menu = Menu.buildFromTemplate([
      {
        label: 'CRM',
        submenu: [
        {
          label: '设置',
          click: function() 
          { 
            console.log('show LoginPage');
            event.sender.send('showPage','loginpage');
          },
          role: 'help'
        }]
      },
      {
        label: '设备',
        role: 'help',
        submenu: [
        {
          label: '外呼',
          click: function () {
            console.log('show phone Page');
            var result = event.sender.send('showPage','phone');
            console.log(result);
          }
        },
        {
          label: '文字转语音',
          click: function () {
            console.log('show tts Page');
            var result = event.sender.send('showPage','tts');
            console.log(result);
          }
        },
        {
          label: '日志信息',
          click: function () {
            //mainWindow.webContents.openDevTools();
            console.log('show displaylog Page');
            var result = event.sender.send('showPage','displaylog');
            console.log(result);
          }
        },
        {
          label: '调试设备',
          click: function () {
            console.log('show debugphone Page');
            var result = event.sender.send('showPage','debugphone');
            console.log(result);
          }
        },
        {
          label: '升级固件',
          click: function () {
            console.log('show updatephone Page');
            var result = event.sender.send('showPage','updatephone');
            console.log(result);
          }
        },{
          label: '打开调试',
          click () {
            mainWindow.webContents.openDevTools();
          }
        }
        ]
      },
      {
        label: '帮助',
        role: 'help',
        submenu: [{
          label: '关于',
          click: function () {
            shell.openExternal('http://www.c3crm.com');
          }
        }]
      }
    ]);

  Menu.setApplicationMenu(menu);
  //mainWindow.hide();
  console.log('CRM Login successfully!');
    
});

//仅防止应用程序被挂起。保持操作系统处于活动状态, 但允许操作系统关闭屏幕
const { powerSaveBlocker } = require('electron');

const id = powerSaveBlocker.start('prevent-app-suspension');


  
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 600, height: 600, autoHideMenuBar:false})

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');
   const {Menu} = require('electron');
  /*隐藏electron创听的菜单栏*/
  Menu.setApplicationMenu(null)

  // Open the DevTools.
  //mainWindow.webContents.openDevTools();

  //系统托盘右键菜单
  var trayMenuTemplate = [
    {
        label: '打开小客',
        click: function () {
          mainWindow.show();
        }
    },
    {
        label: '退出',
         click: function () {
         app.quit();
          app.quit();//因为程序设定关闭为最小化，所以调用两次关闭，防止最大化时一次不能关闭的情况
        }
    }
  ];

  //系统托盘图标目录
  trayIcon = path.join(__dirname, 'images');//app是选取的目录
  appTray = new Tray(path.join(trayIcon, 'crm.png'));//app.ico是app目录下的ico文件     //图标的上下文菜单
  const contextMenu = Menu.buildFromTemplate(trayMenuTemplate);
   //设置此托盘图标的悬停提示内容
  appTray.setToolTip('小客');
   //设置此图标的上下文菜单
  appTray.setContextMenu(contextMenu);
  //单击右下角小图标显示应用
  appTray.on('click',function(){
      mainWindow.show();
  });


  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  mainWindow.on('show', function() {
    if(timer != null) clearInterval(timer);
    appTray.setImage(path.join(trayIcon, 'crm.png'));
    mainWindow.flashFrame(false);
  });
  mainWindow.on('focus', function() {
    if(timer != null) clearInterval(timer);
    appTray.setImage(path.join(trayIcon, 'crm.png'));
    mainWindow.flashFrame(false);
  });
});

