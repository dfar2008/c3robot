let nconf = require('nconf').file({
    file: getUserHome() + '/config.json'
});

function readSettings(settingKey){
    nconf.load();
    return nconf.get(settingKey);
}

function saveSettings(settingKey, settingValue) {
    nconf.set(settingKey, settingValue);
    nconf.save();
}

let getAccessToken = async (accessKeyId, accessKeySecret) => {
  let timestamp = Math.round(new Date().getTime()/1000).toString();
  let accessToken = readSettings('accessToken');
  if (!accessToken || timestamp > (accessToken.ExpireTime + 5 * 60)) {
    var ROAClient = require('@alicloud/pop-core').ROAClient;
    var client = new ROAClient({
      accessKeyId: accessKeyId,
      accessKeySecret: accessKeySecret,
      endpoint: 'http://nls-meta.cn-shanghai.aliyuncs.com',
      apiVersion: '2018-05-18'
    });
    // => returns Promise
    // request(HTTPMethod, uriPath, queries, body, headers, options);
    // options => {timeout}
/*    client.request('POST', '/pop/2018-05-18/tokens').then((result) => {
      saveSettings('accessToken', result.Token);
      saveSettings('apptoken', result.Token.Id);
      console.log(result.Token);
    });   */ 
    let result = await client.request('POST', '/pop/2018-05-18/tokens');
    accessToken = result.Token;
    console.log(accessToken.Token);
    saveSettings('accessToken', accessToken);
    saveSettings('apptoken', accessToken.Id);
    return accessToken.Id;
  }

  return accessToken.Id;

}
function HeartBeatCheck()
{
  webSocket.send('HeartBeatData');
}

const OnSendMessage = (smsnumber, messageContent) => {
  if (smsnumber == '') {
      layer.msg('电话不能为空');
      return;
  }
  if (messageContent == '') {
      layer.msg('短信内容不能为空');
      return;
  }
  webSocket.send('{"command":"SendSMS","arguments":{"phone":"' + smsnumber + '","content":"' + messageContent + '"}}');

}
module.exports = {
  readSettings,
  saveSettings,
  getAccessToken,
  HeartBeatCheck,
  OnSendMessage
}