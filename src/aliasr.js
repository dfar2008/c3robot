function callback(error, response, body) {
    if (error != null) {
        console.log(error);
    }
    else {
        console.log('error: ' + error);
        console.log('response: ' + response);
        console.log('body: ' + body);
        if(!error && response.statusCode == 200) {
        body = JSON.parse(body);
        if (body.status == 20000000) {
            console.log('result: ' + body.result);
            console.log('The audio file recognized succeed!');
            if(body.result.indexOf('不')>-1 || body.result.indexOf('再见')>-1) {
              isLastRecord = true;
              PlayRecord('byebye.wav');
              setTimeout(function () {
                OnPlayStopRecord();                         
                OnHungUp();                      
              },2000);
            }else if(body.result.indexOf('是')>-1 || body.result.indexOf('好')>-1 || body.result.indexOf('可以')>-1 || body.result.indexOf('要')>-1 || body.result.indexOf('有')>-1 || body.result.indexOf('钱')>-1 || body.result.indexOf('多少')>-1){
              isLastRecord = true;
              PlayRecord('weixin.wav');
              setTimeout(function () {
                OnPlayStopRecord();                         
                OnHungUp();                      
              },10000);
            } else {
              isLastRecord = true;
              PlayRecord('contactyoulater.wav');
              setTimeout(function () {
                OnPlayStopRecord();                         
                OnHungUp();                      
              },4000);           
            }
        } else {
            console.log('The audio file recognized failed!');
        }
        }else {
            console.log('The audio file recognized failed');
        }
    }
}
function startASR(audioFile) {
    // if(!fs.existsSync(audioFile)) {
    //     audioFile = 'D:/svn/electron/phonerobot/'+audioFile;
    // }
    var options = getASROptions(audioFile);
    request(options,callback);
}

function getASROptions(audioFile) {
    var appkey = 'XscjJXsviPbtQssk';
    var token = 'b84319d96a484b70b0d71c6d9b07a9b9';
    var requestUrl = 'http://nls-gateway.cn-shanghai.aliyuncs.com/stream/v1/asr';
    if(audioFile == '' || audioFile == null) {
        audioFile = 'D:/svn/electron/phonerobot/recorded.wav';
    }
    var format = 'pcm';
    var sampleRate = '8000';
    var enablePunctuationPrediction = true;
    var enableInverseTextNormalization = true;
    var enableVoiceDetection  = false;
    requestUrl = requestUrl + '?appkey=' + appkey;
    requestUrl = requestUrl + '&format=' + format;
    requestUrl = requestUrl + '&sample_rate=' + sampleRate;
    if (enablePunctuationPrediction) {
        requestUrl = requestUrl + '&enable_punctuation_prediction=' + 'true';
    }
    if (enableInverseTextNormalization) {
        requestUrl = requestUrl + '&enable_inverse_text_normalization=' + 'true';
    }
    if (enableVoiceDetection) {
        requestUrl = requestUrl + '&enable_voice_detection=' + 'true';
    }
    var audioContent = null;
    try {
        audioContent = fs.readFileSync(audioFile);
    } catch(error) {
        if (error.code == 'ENOENT') {
            console.log('The audio file is not exist!');
        }
        return;
    }
    /**
     * 设置HTTP    请求头部
    */
    var httpHeaders = {
        'X-NLS-Token': token,
        'Content-type': 'application/octet-stream',
        'Content-Length': audioContent.length
    };
    var options = {
        url: requestUrl,
        method: 'POST',
        headers: httpHeaders,
        body: audioContent
    };
    return options;
}

function processGETRequest(appkey, token, text, audioSaveFile, format, sampleRate) {
    var url = 'https://nls-gateway.cn-shanghai.aliyuncs.com/stream/v1/tts';
    /**
     * 设置URL请求参数
     */
    url = url + '?appkey=' + appkey;
    url = url + '&token=' + token;
    url = url + '&text=' + text;
    url = url + '&format=' + format;
    url = url + '&sample_rate=' + sampleRate;
    // voice 发音人，可选，默认是xiaoyun
    url = url + "&voice=" + "amei";
    // volume 音量，范围是0~100，可选，默认50
    url = url + "&volume=" + 50;
    // speech_rate 语速，范围是-500~500，可选，默认是0
    // url = url + "&speech_rate=" + 0;
    // pitch_rate 语调，范围是-500~500，可选，默认是0
    // url = url + "&pitch_rate=" + 0;
    console.log(url);
    /**
     * 设置HTTPS GET请求
     * encoding必须设置为null，HTTPS响应的Body为二进制Buffer类型
     */
    var options = {
        url: url,
        method: 'GET',
        encoding: null
    };
    request.get(url).then(response => {
        if (response.ok) {
            let body = response.body;
            fs.writeFileSync(audioSaveFile, body);
            console.log('The GET request is succeed!');
        }
    })

/*    request(options, function (error, response, body) {

        if (error != null) {
            console.log(error);
        }
        else {
            var contentType = response.headers['content-type'];
            if (contentType === undefined || contentType != 'audio/mpeg') {
                console.log(body.toString());
                console.log('The GET request failed!');
            }
            else {
                fs.writeFileSync(audioSaveFile, body);
                console.log('The GET request is succeed!');
            }
        }
    });*/
}
function processPOSTRequest(appkeyValue, tokenValue, textValue, audioSaveFile, formatValue, sampleRateValue) {
    var url = 'https://nls-gateway.cn-shanghai.aliyuncs.com/stream/v1/tts';
    /**
     * 请求参数，以JSON格式字符串填入HTTPS POST 请求的Body中
    */
    var task = {
        appkey : appkeyValue,
        token : tokenValue,
        text : textValue,
        format : formatValue,
        sample_rate : sampleRateValue,
        // voice 发音人，可选，默认是xiaoyun
        voice : 'amei',
        // volume 音量，范围是0~100，可选，默认50
        volume : 10
        // speech_rate 语速，范围是-500~500，可选，默认是0
        // speech_rate : 0,
        // pitch_rate 语调，范围是-500~500，可选，默认是0
        // pitch_rate : 0
    };
    var bodyContent = JSON.stringify(task);
    console.log('The POST request body content: ' + bodyContent);
    /**
     * 设置HTTPS POST请求头部
     */
    var httpHeaders = {
        'Content-type' : 'application/json'
    }
    /**
     * 设置HTTPS POST请求
     * encoding必须设置为null，HTTPS响应的Body为二进制Buffer类型
     */
    var options = {
        url: url,
        method: 'POST',
        headers: httpHeaders,
        body: bodyContent,
        encoding: null
    };
    request(options, function (error, response, body) {
        /**
         * 处理服务端的响应
         */
        if (error != null) {
            console.log(error);
        }
        else {
            var contentType = response.headers['content-type'];
            if (contentType === undefined || contentType != 'audio/mpeg') {
                console.log(body.toString());
                console.log('The POST request failed!');
            }
            else {
                fs.writeFileSync(audioSaveFile, body);
                console.log('The POST request is succeed!');
            }
        }
    });
}
// var appkey = '您的appkey';
// var token = '您的token';
// var text = '今天是周一，天气挺好的。';
// var textUrlEncode = encodeURIComponent(text)
//                     .replace(/[!'()*]/g, function(c) {
//                         return '%' + c.charCodeAt(0).toString(16);
//                     });
// console.log(textUrlEncode);
// var audioSaveFile = 'syAudio.wav';
// var format = 'wav';
// var sampleRate = 16000;
// processGETRequest(appkey, token, textUrlEncode, audioSaveFile, format, sampleRate);
// processPOSTRequest(appkey, token, text, audioSaveFile, format, sampleRate);