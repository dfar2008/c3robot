let { readSettings }  = require('./utils');

const request = require('superagent');
let src_crmurl = readSettings('crmurl');
let serialNumber  = readSettings('serialnumber');

request.serialize['application/xml'] = function (obj) {
    return 'string generated from obj';
};

let getCallStepsListRequest = () => {
  let userid = readSettings('userid');
  let data = {
    session: userid,
    module_name: 'mobileCallSteps',
    serialnumber: serialNumber

  };
  let rest_data = JSON.stringify(data);
  let url = src_crmurl + '/rest.php?method=getCallStepsList&fromJava=2&rest_data=' + rest_data;
  return request
        .post(url)
        .query({
          method: 'getCallStepsList',
          fromJava: 2,
          rest_data: rest_data
        })
        .accept('json');
}

let changeCallLogsStatusRequest = (callLogsid, status) => {
  let userid = readSettings('userid');
  let data = {
    session: userid,
    module_name: 'mobileCallSteps',
    id: callLogsid,
    status: status
  };
  let rest_data = JSON.stringify(data);
  let url = src_crmurl + '/rest.php';
  return request
        .post(url)
        .query({
          method: 'changeCallLogsStatus',
          fromJava: 2,
          rest_data: rest_data
        })
        .accept('json');
}

let createCallChatLogsRequest = ({calllogsid = 0, content = '', iscustomer = 0, type, callee = 0, duringtime = 0, state = 2}) => {
  let userid = readSettings('userid');
  if (!calllogsid || !callee) {
    throw new Error('参数不符合');
  }
  let data = {
    session: userid,
    module_name: 'mobileCallSteps',
    calllogsid: calllogsid,
    chattxt: content,
    iscustomer: iscustomer,
    type: type,
    callee: callee,
    duringtime: duringtime,
    state: state
  };
  let rest_data = JSON.stringify(data);
  let url = src_crmurl + '/rest.php';
  return request
        .post(url)
        .query({
          method: 'createCallChatLogs',
          fromJava: 2,
          rest_data: rest_data
        })
        .accept('json');
}

let getCallLogsStatisticsRequest = () => {
  let userid = readSettings('userid');
  let data = {
    session: userid,
    module_name: 'mobileCallSteps',
  };
  let rest_data = JSON.stringify(data);
  let url = src_crmurl + '/rest.php';
  return request
        .post(url)
        .query({
          method: 'getCallLogsStatistics',
          fromJava: 2,
          rest_data: rest_data
        })
        .accept('json');  
}

module.exports = {
  getCallStepsListRequest,
  changeCallLogsStatusRequest,
  createCallChatLogsRequest,
  getCallLogsStatisticsRequest
}